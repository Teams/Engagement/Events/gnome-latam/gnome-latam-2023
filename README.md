# GNOME Latam 2023
https://events.gnome.org/event/136/
```
Evento Virtual al que todos pueden unirse del 26 al 28 de mayo de 2023.
```
```
evento virtual no qual todos podem participar de 26 a 28 de maio de 2023.
```

## Resumen / Resumo

- Motivados por el éxito de eventos virtuales como GNOME Asia y GNOME Onboard Africa, decidimos organizar una conferencia GNOME LATAM (virtual).
- Son un par de días para celebrar y expandir la comunidad GNOME en América Latina. Ven a conocer y compartir experiencias en la creación y uso de tecnologías GNOME en nuestra región.


- Motivados pelo sucesso de eventos virtuais como GNOME Asia e GNOME Onboard Africa, decidimos organizar uma conferência GNOME LATAM (virtual).
- São alguns dias para celebrar e expandir a comunidade GNOME na América Latina. Venha conhecer e compartilhar experiências na criação e uso das tecnologias GNOME em nossa região.

## ¿Cuál es el plan para 2023? / ¿Qual é o plano para 2023?

### Desafios

- Latam es una región vasta, en su mayoría desconectada.
- Viajar entre países de esta región es costoso debido a las grandes distancias.
- Las comunidades locales casi no tienen apoyo financiero para viajar.
- Las olas de COVID todavía van y vienen, y la vacunación está muy atrasada.

### Desafios

- A América Latina é uma região vasta, em sua maioria desconectada.
- O deslocamento entre os países desta região é caro devido às grandes distâncias.
- As comunidades locais quase não têm apoio financeiro para viagens.
- As ondas de COVID ainda vêm e vão, e a vacinação está muito atrasada.

### El plan

- Un nuevo enfoque híbrido.
- Un evento virtual principal al que todos pueden unirse del 26 al 28 de mayo de 2023.
- Dirigido a hablantes de español y portugués de nuestra región, pero abierto también para otras regiones.
- Múltiples eventos centrales locales más pequeños en diferentes ciudades durante ambos días.
- Un hub local (de referencia) de tamaño medio en Zacatecas, México. “GNOME Latam 2023: Presentando a Zacatecas México, del 25 de mayo al domingo 28 de 2023.”
Cada centro local proporcionará su propio equipo de organización local.

### O plano
- Uma nova abordagem híbrida.
- Um evento virtual principal ao qual todos podem participar de 26 a 28 de maio de 2023.
- Direcionado a falantes de espanhol e português da nossa região, mas também aberto a outras regiões.
- Vários eventos de hub locais menores em diferentes cidades durante os dois dias.
- Um hub local de tamanho médio (referência) em Zacatecas, México, por exemplo “GNOME Latam 2023: apresentando Zacatecas México, de 25 de maio a domingo, 28 de 2023.”
Cada hub local fornecerá sua própria equipe de organização local.

## Organizadores Principales / Principais Organizadores

- [Martín Abente Lahaye](https://gitlab.gnome.org/tchx84) Foundation Member and GNOME Board Director.
- [Manuel Haro](https://gitlab.gnome.org/mharo) Foundation Member and GUADEC 2022 organizer.
- [Federico Mena Quintero](https://gitlab.gnome.org/federico) Foundation Member and GNOME co-founder.
- [Felipe Borges](https://gitlab.gnome.org/felipeborges) Foundation Member, former GNOME Board Director, and Google Summer of Code organization admin.
- [Manuel Quiñoes](https://gitlab.gnome.org/manuq) Foundation Member and Endless Foundation.
- [Daniel Galleguillos Cruz](https://gitlab.gnome.org/dnlgalleguillos) Foundation Member and Engagement Team.
